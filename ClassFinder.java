package unittest_classfinder;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ClassFinder {
	/*This method will take 2 arguments first is full path of text file and second argument is the pattern*/
	public static void searchClassName(String filePath,String pattern) throws Exception
    {
        File inputDataFile = new File(filePath);
        Scanner inFile = new Scanner(inputDataFile);
        int length = inFile.nextInt();
        String names[] = new String[length];
        ArrayList<String> fileList = new ArrayList<String>();
        for(int i=0;i<length;i++){
            names[i] = inFile.nextLine();
        }
            String input = pattern;
            byte[] inputBytes = input.getBytes("US-ASCII");

            for(int i = 0;i<length;i++){

                byte[] fileBytes = names[i].getBytes("US-ASCII");
                int count=0;
                for(int z=0;z<inputBytes.length;z++)
                {
                    if(Bytes.contains(fileBytes, inputBytes[z]))
                    {
                        count++;
                        if(count==inputBytes.length)
                        {
                            fileList.add(names[i]);
                        }
                    }
                }
            }

        for(int i=0;i<fileList.size();i++)
        {
            System.out.println("These Classes Found:  "+ fileList.get(i));
        }
        if(fileList.size() == 0)
        {
            System.out.println("No class found with This pattern!");
        }
    }
	
	public static void main(String[] args) {
		try {			
            searchClassName("C:\\Users\\Shoaib.Aamir\\Desktop\\test1.txt","Stu");
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
}
